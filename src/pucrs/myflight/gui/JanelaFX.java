package pucrs.myflight.gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.*;

import javax.swing.SwingUtilities;

import javafx.collections.FXCollections;
import javafx.scene.control.*;
import org.jxmapviewer.JXMapViewer;
import org.jxmapviewer.viewer.GeoPosition;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pucrs.myflight.modelo.*;

public class JanelaFX extends Application {

	final SwingNode mapkit = new SwingNode();

	private GerenciadorCias gerCias;
	private GerenciadorAeroportos gerAero;
	private GerenciadorRotas gerRotas;
	private GerenciadorAeronaves gerAvioes;
	private GerenciadorPaises gerPaises;

	private GerenciadorMapa gerenciador;

	private EventosMouse mouse;

	private ObservableList<CiaAerea> comboCiasData;
	private ComboBox<CiaAerea> comboCia;
    private TextArea output;

    @Override
	public void start(Stage primaryStage) throws Exception {

		setup();

		GeoPosition poa = new GeoPosition(-30.05, -51.18);
		gerenciador = new GerenciadorMapa(poa, GerenciadorMapa.FonteImagens.VirtualEarth);
		mouse = new EventosMouse();
		gerenciador.getMapKit().getMainMap().addMouseListener(mouse);
		gerenciador.getMapKit().getMainMap().addMouseMotionListener(mouse);

		createSwingContent(mapkit);

		BorderPane pane = new BorderPane();
		GridPane leftPane = new GridPane();
		GridPane bottomPane = new GridPane();

		leftPane.setAlignment(Pos.CENTER);
		leftPane.setHgap(10);
		leftPane.setVgap(10);
		leftPane.setPadding(new Insets(10, 10, 10, 10));

		bottomPane.setAlignment(Pos.BOTTOM_CENTER);
		bottomPane.setHgap(2);
		bottomPane.setVgap(2);
		bottomPane.setPadding(new Insets(2, 2, 2, 2));

		Button btnConsulta1 = new Button("Fluxo no mundo");
		Button btnConsulta2 = new Button("Aeroportos e Fluxo de Rotas ");
		Button btnConsulta3 = new Button("Rotas Possíveis");
		Button btnConsulta4 = new Button("Vôos Alcançaveis");
		Button btnConsulta5 = new Button("Fluxo por país ");

		ComboBox<String> comboPaises = new ComboBox<>();
		ComboBox<String> comboCias = new ComboBox<>();


		TextField origemConsulta3 = new TextField("GRU");
		TextField destinoConsulta3 = new TextField("POA");
		TextField origemConsulta4 = new TextField("GRU");
		TextField horasConsulta4 = new TextField("2.0");

		Label labelConsulta3 = new Label("Rotas Possiveis Entre (COD)");
		Label labelConsulta4 = new Label("Rotas partindo de (COD) (H)");

		leftPane.add(btnConsulta1, 0, 0);
		leftPane.add(btnConsulta5, 0, 1);
		leftPane.add(btnConsulta2, 1, 0);
		leftPane.add(btnConsulta3, 2, 0);
		leftPane.add(btnConsulta4, 3, 0);

		bottomPane.add(comboPaises, 0, 1);
		bottomPane.add(comboCias, 1,1);
		bottomPane.add(labelConsulta3, 2,0);
		bottomPane.add(origemConsulta3, 2,1);
		bottomPane.add(destinoConsulta3, 2,2);
		bottomPane.add(labelConsulta4, 3,0);
		bottomPane.add(origemConsulta4, 3,1);
		bottomPane.add(horasConsulta4, 3,2);


		List<String> cias = new ArrayList<>();
		for(CiaAerea c : gerCias.listarTodasOrdenadaPorNome()) {
			cias.add(c.getNome());
		}
		comboCias.setItems(FXCollections.observableArrayList(cias));
		comboCias.getItems().add(0, cias.get(0));
		comboCias.getSelectionModel().selectFirst();

		List<String> paises = new ArrayList<>();
		for(Pais p : gerPaises.listarTodos()) {
			paises.add(p.getNome());
		}
		comboPaises.setItems(FXCollections.observableArrayList(paises));
		comboPaises.getItems().add(0, paises.get(0));
		comboPaises.getSelectionModel().selectFirst();

		btnConsulta1.setOnAction(e -> {
			consulta1(comboCias.getValue());
		});

        btnConsulta2.setOnAction(e -> {
            consulta2();
        });

        btnConsulta3.setOnAction(e -> {
            consulta3(origemConsulta3.getText(), destinoConsulta3.getText());
        });

		btnConsulta4.setOnAction(e -> {
		    consulta4(origemConsulta4.getText(), horasConsulta4.getText(), (double) 867);
        });

		btnConsulta5.setOnAction(e -> {
			consulta5(comboPaises.getValue());
		});

		pane.setCenter(mapkit);
		pane.setTop(leftPane);
		pane.setBottom(bottomPane);

		Scene scene = new Scene(pane, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Mapas com JavaFX");
		primaryStage.show();
	}

	// Inicializando os dados aqui...
	private void setup() {

		gerCias = new GerenciadorCias();
		gerAero = new GerenciadorAeroportos();
		gerRotas = new GerenciadorRotas();
		gerAvioes = new GerenciadorAeronaves();
		gerPaises = new GerenciadorPaises();

        try {
            System.out.println("Carregando países");
            gerPaises.carregaDados();
        } catch (IOException e) {
            System.err.println("Não foi possível ler countries.dat!");
        }

        try {
            System.out.println("Carregando aviões");
            gerAvioes.carregaDados();
        } catch (IOException e) {
            System.err.println("Não foi possível ler equipment.dat!");
        }

        try {
            System.out.println("Carregando companhias");
            gerCias.carregaDados();
        } catch (IOException e) {
            System.err.println("Não foi possível ler airlines.dat!");
        }

        try {
            System.out.println("Carregando aeroportos");
            gerAero.carregaDados();
        } catch (IOException e) {
            System.err.println("Não foi possível ler airports.dat!");
        }

        try {
            System.out.println("Carregando rotas");
            gerRotas.carregaDados(gerCias, gerAero, gerAvioes);
        } catch (IOException e) {
            System.err.println("Não foi possível ler routes.dat!");
        }
	}
//  1 Desenhar todos os aeroportos onde uma determinada companhia aérea opera. Mostre também as rotas envolvidas.
	private void consulta1(String cia) {
        gerenciador.clear();

        // Lista para armazenar o resultado da consulta
        List<MyWaypoint> lstPoints = new ArrayList<>();
		Tracado tr = null;

        for(Rota rota : gerRotas.listarTodas()) {

        	if(rota.getCia().getNome().equalsIgnoreCase(cia)) {
				Aeroporto aeroportoOrigem = gerAero.buscarCodigo(rota.getOrigem().getCodigo());
				Aeroporto aeroportoDestino = gerAero.buscarCodigo(rota.getDestino().getCodigo());

				tr = new Tracado();
				tr.setLabel("De "+aeroportoOrigem.getNome() +" para "+ aeroportoDestino.getNome());
				tr.setWidth(2);
				tr.setCor(new Color(0,0,255));

				tr.addPonto(aeroportoOrigem.getLocal());
				tr.addPonto(aeroportoDestino.getLocal());

				gerenciador.addTracado(tr);

				lstPoints.add(new MyWaypoint(Color.RED, aeroportoOrigem.getNome(), aeroportoOrigem.getLocal(), 5));
				lstPoints.add(new MyWaypoint(Color.RED, aeroportoDestino.getNome(), aeroportoDestino.getLocal(), 5));
			}
        }

        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint();


    }

//	2- Exibir uma estimativa de volume de tráfego de todos os aeroportos ou de um país específico.
//	Deve-se explorar o tamanho e cor do ponto correspondente, para destacar aeroportos com maior ou menor tráfego.
	private void consulta2(){
		gerenciador.clear();

		List<MyWaypoint> lstPoints = new ArrayList<>();

		gerRotas.ordenarNomesAeroportos();
		String codAeroporto = "";  //criado para nivel de comparação depos
		for (Rota rota : gerRotas.listarTodas()){
			Aeroporto aeroportoOrigem = gerAero.buscarCodigo(rota.getOrigem().getCodigo()); //guarda o cod do aeroporto

			if(codAeroporto.equalsIgnoreCase(aeroportoOrigem.getCodigo())) { //se é o mesmo lugar
				for(MyWaypoint myWaypoint : lstPoints) { //percorre a lista de pontos
					if (myWaypoint.getLabel().equalsIgnoreCase(aeroportoOrigem.getCodigo())) { //label é o cod, se é o mesmo
						// que o cod do aeroporto
						double size = lstPoints.get(lstPoints.indexOf(myWaypoint)).getSize();//posição do ponto
						lstPoints.remove(myWaypoint); // remove a ponto da posição
						size++; //crementa o tamanho

						//condicionais para mudança de cor do ponto
						if (size < 10) {
							lstPoints.add(new MyWaypoint(Color.green, aeroportoOrigem.getCodigo(), aeroportoOrigem.getLocal(), size));
						} else if (size < 25)
							lstPoints.add(new MyWaypoint(Color.yellow, aeroportoOrigem.getCodigo(), aeroportoOrigem.getLocal(), size));
						else {
							lstPoints.add(new MyWaypoint(Color.red, aeroportoOrigem.getCodigo(), aeroportoOrigem.getLocal(), size));
						}
						//condições

					}

				}
			} else { //caso ele não tenho ponto ainda ele add
				codAeroporto = aeroportoOrigem.getCodigo();
				lstPoints.add(new MyWaypoint(Color.RED, aeroportoOrigem.getCodigo(), aeroportoOrigem.getLocal(), 1));
			}
		}

		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();  //atualiza o mapa
	}

    private void consulta3(String codAeroOrigem, String codAeroDestino) {
        gerenciador.clear(); // limpa mapa

        if (codAeroOrigem == null) codAeroOrigem = "GRU"; // valor padrão pra fins de teste
        if (codAeroDestino == null) codAeroDestino = "POA";

        Aeroporto aeroOrigem = gerAero.buscarCodigo(codAeroOrigem); // Salva referencia pro Aeroporto de Origem para podermos comparar depois
        Aeroporto aeroDestino = gerAero.buscarCodigo(codAeroDestino); // Salva referencia pro Aeroporto de Origem

		if(aeroOrigem == null || aeroDestino == null) { // se origem ou destino não forem encontrados
			System.err.println("Não foi possivel realizar consulta. Busca do aeroporto de origem ou de destino veio inválida pelos codigos informados.");
		}

        ArrayList<Rota> rotasDeOrigem = gerRotas.buscarOrigem(codAeroOrigem); // busca todas as rotas onde a partida seja o aeroporto de origem



        List<MyWaypoint> lstPoints = new ArrayList<>();

        HashSet<Aeroporto> aeroportosPassadosPor = new HashSet<>(); // Usado para indicar por quais aeroportos já passamos por. HashSet: Elementos não são ordenados, O(1) então tempo pra remover ou adicionar sempre é o mesmo // https://www.devmedia.com.br/diferencas-entre-treeset-hashset-e-linkedhashset-em-java/29077
        aeroportosPassadosPor.add(aeroOrigem);
        aeroportosPassadosPor.add(aeroDestino);

        for(Rota r : rotasDeOrigem){ // para cada rota partindo da origem

            if(r.getDestino().equals(aeroDestino)) { // Se houver uma rota direto da origem para o destino

                Tracado tr = new Tracado(); // cria um novo traçado
                tr.setCor(Color.RED); // vermelho indicando que é uma conexão direta
                tr.setWidth(5);
                tr.addPonto(aeroOrigem.getLocal()); // onde a origem é o aeroporto de partida
                tr.addPonto(aeroDestino.getLocal()); // e o destino é o destino final
                gerenciador.addTracado(tr); // adiciona o traçado no mapa

            } else { // caso não vá até o destino
                ArrayList<Rota> rotasConexao = gerRotas.buscarOrigem(r.getDestino().getCodigo()); // pegamos uma lista com todas as rotas partindo dessa possivel conexão
                for(Rota rc : rotasConexao){ // para cada rota dessa lista de possiveis conexões
                    if(rc.getDestino().equals(aeroDestino)){ // se o destino dessa possivel conexão for o aeroporto desejado
                        if(!aeroportosPassadosPor.contains(rc.getOrigem())) { // verifica se já não adicionamos à lista
                            aeroportosPassadosPor.add(rc.getOrigem()); // adicionamos esse aeroporto conexão à lista de aeroportos
                        }

                        // Adicionamos traçado até essa conexão
                        Tracado tr = new Tracado();
                        tr.setCor(Color.BLUE); // azul para indireta
                        tr.setWidth(1);
                        tr.addPonto(r.getOrigem().getLocal()); // da origem
                        tr.addPonto(r.getDestino().getLocal()); // até a conexão
                        gerenciador.addTracado(tr);

                        tr.clear(); // limpamos o traçado para reutilizar

                        // adicionamos traçado até o destino
                        tr.setCor(Color.BLUE);
                        tr.setWidth(2);
                        tr.addPonto(rc.getOrigem().getLocal()); // da conexão
                        tr.addPonto(rc.getDestino().getLocal()); // até o destino final
                        gerenciador.addTracado(tr); // adicionamos o traçado
                    }
                }
            }
        }

        for (Aeroporto a: aeroportosPassadosPor) {
            if(a.equals(aeroOrigem)) { // se for o aeroporto de origem
                lstPoints.add(new MyWaypoint(Color.RED, a.getNome(), a.getLocal(), 10));
            } else if (a.equals(aeroDestino)) { // se for o aeroporto de destino
                lstPoints.add(new MyWaypoint(Color.RED, a.getNome(), a.getLocal(), 10));
            } else { // se for uma conexão que leve até o destino
                lstPoints.add(new MyWaypoint(Color.BLUE, a.getNome(), a.getLocal(), 6));
            }
        }

        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint();

    }

    private void consulta4(String codAero, String stringHoras, Double kmporhora) { // kmporhora = null na hora
        //    private void consulta4(String codAero, double horas, Double kmporhora) { // kmporhora = null na hora

        double horas = 0;
        try {
            horas = Double.parseDouble(stringHoras); // converte pra numeros
        } catch (Exception e) { // se der erro
            System.err.println("Valor inserido não é numero");
            return; // para tudo
        }

        if(kmporhora == null) {
            kmporhora = 867.00;
        }
        gerenciador.clear(); // limpa mapa

        if (codAero == null) return; // valor padrão pra fins de teste

        Aeroporto aeroOrigem = gerAero.buscarCodigo(codAero); // Salva referencia pro Aeroporto de Origem para podermos comparar depois

        if(aeroOrigem == null) { // se origem ou destino não forem encontrados
            System.err.println("Não foi possivel realizar consulta. Busca do aeroporto de origem é inválido pelos codigos informados.");
        }

        ArrayList<Rota> rotasDeOrigem = gerRotas.buscarOrigem(codAero); // busca todas as rotas onde a partida seja o aeroporto de origem

        List<MyWaypoint> lstPoints = new ArrayList<>();

        HashSet<Aeroporto> aeroportosPassadosPor = new HashSet<>(); // Usado para indicar por quais aeroportos já passamos por. HashSet: Elementos não são ordenados, O(1) então tempo pra remover ou adicionar sempre é o mesmo // https://www.devmedia.com.br/diferencas-entre-treeset-hashset-e-linkedhashset-em-java/29077
        aeroportosPassadosPor.add(aeroOrigem);

        for(Rota r : rotasDeOrigem){ // para cada rota partindo da origem
            Geo geoOrigem = r.getOrigem().getLocal();
            Geo geoDestino = r.getDestino().getLocal();

            if(geoDestino == null || geoOrigem == null) continue;
            System.out.println(horas);
            Double distancia = Geo.distancia(geoOrigem, geoDestino);
            double duracaoRota = (distancia/kmporhora);
            if(horas - duracaoRota > 0) { // se duracao for superior

                Tracado tr = new Tracado(); // cria um novo traçado
                tr.setCor(Color.BLUE); // vermelho indicando que é uma conexão direta
                tr.setWidth(1);
                tr.addPonto(r.getOrigem().getLocal()); // onde a origem é o aeroporto de partida
                tr.addPonto(r.getDestino().getLocal()); // e o destino é o destino final
                gerenciador.addTracado(tr); // adiciona o traçado no mapa
                // caso não vá até o destino
                ArrayList<Rota> rotasConexao = gerRotas.buscarOrigem(r.getDestino().getCodigo()); // pegamos uma lista com todas as rotas partindo dessa possivel conexão
                for(Rota rc : rotasConexao){ // para cada rota dessa lista de possiveis conexões
                    double restanteHoras = (horas - duracaoRota);
                    Geo geoOrigem2 = rc.getOrigem().getLocal();
                    Geo geoDestino2 = rc.getDestino().getLocal();

                    if(geoDestino2 == null || geoOrigem2 == null) continue;
                    System.out.println(restanteHoras);
                    Double distancia2 = Geo.distancia(geoOrigem2, geoDestino2);
                    double duracaoConexao = (distancia2/kmporhora);
                    if((restanteHoras - duracaoConexao) > 0){ // se o destino dessa possivel conexão for o aeroporto desejado
                        if(!aeroportosPassadosPor.contains(rc.getOrigem())) { // verifica se já não adicionamos à lista
                            aeroportosPassadosPor.add(rc.getOrigem()); // adicionamos esse aeroporto conexão à lista de aeroportos
                        }

                        // Adicionamos traçado até essa conexão
                        tr = new Tracado();
                        tr.setCor(Color.BLUE); // azul para indireta
                        tr.setWidth(1);
                        tr.addPonto(r.getOrigem().getLocal()); // da origem
                        tr.addPonto(r.getDestino().getLocal()); // até a conexão
                        gerenciador.addTracado(tr);

                        tr.clear(); // limpamos o traçado para reutilizar

                        // adicionamos traçado até o destino
                        tr.setCor(Color.BLUE);
                        tr.setWidth(1);
                        tr.addPonto(rc.getOrigem().getLocal()); // da conexão
                        tr.addPonto(rc.getDestino().getLocal()); // até o destino final
                        gerenciador.addTracado(tr); // adicionamos o traçado
                    }
                }
            }
        }

        for (Aeroporto a: aeroportosPassadosPor) {
            if(a.equals(aeroOrigem)) { // se for o aeroporto de origem
                lstPoints.add(new MyWaypoint(Color.BLUE, a.getNome(), a.getLocal(), 6));
            } else { // se for uma conexão que leve até o destino
                lstPoints.add(new MyWaypoint(Color.BLUE, a.getNome(), a.getLocal(), 6));
            }
        }

        gerenciador.setPontos(lstPoints);
        gerenciador.getMapKit().repaint();

    }

	private void consulta5(String pais){
        if (pais == null) {
            pais = "BR";
        }

		gerenciador.clear();

		List<MyWaypoint> lstPoints = new ArrayList<>();

		gerRotas.ordenarNomesAeroportos();
		for (Rota rota : gerRotas.listarTodas()) {
			Pais p = gerPaises.buscarCodigo(rota.getOrigem().getCodPais());

			if(p.getNome().equalsIgnoreCase(pais)) {
				lstPoints.add(new MyWaypoint(Color.RED, p.getCod(), rota.getOrigem().getLocal(), 1));
			}
		}


		gerenciador.setPontos(lstPoints);
		gerenciador.getMapKit().repaint();  //atualiza o mapa
	}

	/*
	    ============================ CONSULTA 4 ============================
	*/
	private void consulta4b(String codAero, String stringHoras, Double kmporhora) { // kmporhora = null na hora
    //    private void consulta4(String codAero, double horas, Double kmporhora) { // kmporhora = null na hora

		double horas = 0;
		try {
			horas = Double.parseDouble(stringHoras); // converte pra numeros
		} catch (Exception e) { // se der erro
			System.err.println("Valor inserido não é numero");
			return; // para tudo
		}

	    if(kmporhora == null) {
	        kmporhora = 867.00;
        }

	    Aeroporto aeroDeOrigem = gerAero.buscarCodigo(codAero);
	    System.out.println(aeroDeOrigem.toString());

	    if (aeroDeOrigem == null) {
	        System.err.println("Nenhum aeroporto com esse codigo");

        }

	    ArrayList<Rota> rotasPartindoDaOrigem = gerRotas.buscarOrigem(aeroDeOrigem.getCodigo()); // Tá vindo uma lista vazia

        System.out.println("Rotas partindo da origem:"+rotasPartindoDaOrigem.size());
        System.out.println(gerRotas.listarTodas());
        for(Rota r: gerRotas.listarTodas()){
            if(!r.getOrigem().getCodigo().equals(aeroDeOrigem.getCodigo())){
                rotasPartindoDaOrigem.add(r);
                System.out.println("Adicionando rota: "+r);
            }
        }

	    ArrayList<Rota> resultado = new ArrayList<>();

        HashSet<Aeroporto> aeroportosPassadosPor = new HashSet<>();
        aeroportosPassadosPor.add(aeroDeOrigem);

        ArrayList<Geo> rotasPossiveis = new ArrayList<>();
	    for (Rota origemParaEscala: rotasPartindoDaOrigem) {
            System.out.println(">>>>"+origemParaEscala.getOrigem());
	        Geo geoOrigem = origemParaEscala.getOrigem().getLocal();
	        Geo geoDestino = origemParaEscala.getDestino().getLocal();

	        if(geoDestino == null || geoOrigem == null)
	            continue;
               System.out.println(horas);
            Double distancia = Geo.distancia(geoOrigem, geoDestino);
	        double duracaoRota = Math.round((distancia/867.0)*60);
            System.out.println("" + duracaoRota);
            if (duracaoRota > horas){
                System.out.println("Tempo insuficiente para o voo e sem gasolina avião n voa!");
                System.out.println("Próxima Rota");

                if (duracaoRota < horas) {
                    System.out.println("ufa temos gasolina");
                    horas = horas - duracaoRota;
                    rotasPossiveis.add(geoOrigem);

                }
                System.out.println(rotasPossiveis);
                continue;
            }
            System.out.println(duracaoRota);
	        //double tempoRestante = hora - duracaoRota;
            if(rotasPossiveis.size()>=2){
                System.out.println("O limite de conexões foi atingido, pulem da aeronave!");
                break;
            }
            if(duracaoRota <= horas) {
                resultado.add(origemParaEscala); // adiciona rota se houver horas suficientes para chegar até la
                aeroportosPassadosPor.add(origemParaEscala.getDestino()); // adicionando na lista de aeroportos passados por
                double horasRemanescentes = horas - duracaoRota;
                for (Rota escalaParaDestino: gerRotas.buscarOrigem(origemParaEscala.getDestino().getCodigo())) {
                    Geo geoOrigemEscala = escalaParaDestino.getOrigem().getLocal();
                    Geo geoDestinoEscala = escalaParaDestino.getDestino().getLocal();
                    double duracaoEscala = Geo.distancia(geoOrigemEscala, geoDestinoEscala)/kmporhora;
                    if(duracaoRota <= horasRemanescentes && escalaParaDestino.getDestino() != aeroDeOrigem) { // adiciona rota se houver horas remanescentes suficientes para chegar até la e se o destino não for a origem
                        resultado.add(escalaParaDestino);
                        aeroportosPassadosPor.add(escalaParaDestino.getDestino()); // adicionando na lista de aeroportos passados por
                    }
                }
            }
        }

        gerenciador.clear();

        // Lista para armazenar o resultado da consulta
        List<MyWaypoint> lstPoints = new ArrayList<>();

        aeroportosPassadosPor.forEach(a -> {
            MyWaypoint novo = new MyWaypoint(Color.RED, a.getCodigo(), a.getLocal(), 30); // cor, nome, geo local, tamanho do ponto
            lstPoints.add(novo);
        });
		gerenciador.setPontos(lstPoints);

        resultado.forEach(rota -> {
            Aeroporto aeroportoOrigem = rota.getOrigem(); //gerAero.buscarCodigo(rota.getOrigem().getCodigo());
            Aeroporto aeroportoDestino = rota.getDestino(); //gerAero.buscarCodigo(rota.getDestino().getCodigo());

            Tracado tr = new Tracado();
            tr.setLabel("De "+aeroportoOrigem.getNome() +" para "+ aeroportoDestino.getNome());
            tr.setWidth(5);
            tr.setCor(new Color(0,0,0,60));

            tr.addPonto(aeroportoOrigem.getLocal());
            tr.addPonto(aeroportoDestino.getLocal());


            gerenciador.addTracado(tr);
        });

        System.out.println(resultado);
        System.out.println("==========================");
        System.out.println(aeroportosPassadosPor);

		gerenciador.getMapKit().repaint();
	}

	private class EventosMouse extends MouseAdapter {
		private int lastButton = -1;

		//=================================================================================================

		@Override
		public void mousePressed(MouseEvent e) {
			JXMapViewer mapa = gerenciador.getMapKit().getMainMap();
			GeoPosition loc = mapa.convertPointToGeoPosition(e.getPoint());
			// System.out.println(loc.getLatitude()+", "+loc.getLongitude());
			lastButton = e.getButton();
			// Botão 3: seleciona localização
			if (lastButton == MouseEvent.BUTTON3) {
				gerenciador.setPosicao(loc);
				gerenciador.getMapKit().repaint();
			}
		}
	}

	private void createSwingContent(final SwingNode swingNode) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				swingNode.setContent(gerenciador.getMapKit());
			}
		});
	}

	public static void main(String[] args) {
		launch(args);
	}
}
