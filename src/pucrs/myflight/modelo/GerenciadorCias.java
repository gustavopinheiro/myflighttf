package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GerenciadorCias {
    private HashMap<String, CiaAerea> empresas;

    public GerenciadorCias() {
        this.empresas = new HashMap<>();
    }

    public ArrayList<CiaAerea> listarTodas() {
        return new ArrayList<>(empresas.values());
    }

    public void carregaDados() throws IOException {
        Path path = Paths.get("airlines.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            String header = sc.nextLine(); // pula cabeçalho
            String cod, nome;
            while (sc.hasNext()) {
                cod = sc.next();
                nome = sc.next().replaceAll("(\r)", "");
                CiaAerea nova = new CiaAerea(cod, nome);
                adicionar(nova);
                System.out.println("Carregando no Gerenciador de Companhias Aereas: " + nova);
            }
        }
    }

    public void adicionar(CiaAerea cia1) {
        empresas.put(cia1.getCodigo(),
                cia1);
    }

    public CiaAerea buscarCodigo(String cod) {
        return empresas.get(cod);
    }

    public CiaAerea buscarNome(String nome) {
        for(CiaAerea cia: empresas.values())
           if(cia.getNome().equals(nome))
               return cia;
        return null;
    }

    public ArrayList<CiaAerea> listarTodasOrdenadaPorNome() {
        ArrayList<CiaAerea> lista = listarTodas();
        Comparator.comparing(CiaAerea::getNome);
        return lista;
    };
}
