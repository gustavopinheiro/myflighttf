package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GerenciadorAeronaves {

    private HashMap<String, Aeronave> avioes;

    public GerenciadorAeronaves() {
        this.avioes = new HashMap<>();
    }

    public void carregaDados() throws IOException {
        Path path = Paths.get("equipment.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            String header = sc.nextLine(); // pula cabeçalho
            String cod, descricao;
            int cap;
            while (sc.hasNext()) {
                cod = sc.next();
                descricao = sc.next();
                cap = Integer.parseInt(sc.next().replaceAll("(\r)", "")); // tem um \r escondido que impede não deixa usar o sc.nextInt()
                Aeronave nova = new Aeronave(cod, descricao, cap);
                adicionar(nova);
                System.out.println("Carregando Aeronave no Gerenciador de Aeronaves: " + nova);
            }
        }
    }

    public void adicionar(Aeronave aviao) {
        avioes.put(aviao.getCodigo(), aviao);
    }

    public ArrayList<Aeronave> listarTodas() {
        return new ArrayList<>(avioes.values());
    }

    public Aeronave buscarCodigo(String codigo) {
        return avioes.get(codigo);
    }
}
