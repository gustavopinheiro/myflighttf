package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;



public class GerenciadorRotas {

    private ArrayList<Rota> rotas;

    public GerenciadorRotas() {
        this.rotas = new ArrayList<>();
    }

    public void carregaDados(GerenciadorCias gerCias, GerenciadorAeroportos gerAero, GerenciadorAeronaves gerAvioes) throws IOException {
        //System.out.println("### Carregando Rotas ###\n\n");
        Path path2 = Paths.get("routes.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n ]"); // separadores: ; e nova linha separa por espaço também por causa que as vezes tem várias aeronaves
            String header = sc.nextLine(); // pula cabeçalho
            String cod, codAeroOrigem, codAeroDestino, codAeronaves;
            while (sc.hasNext()) {
                cod = sc.next();
                codAeroOrigem = sc.next();
                codAeroDestino = sc.next();
                sc.next(); // codeshare
                sc.next(); // stops
                codAeronaves = sc.next().replaceAll("(\r)", ""); // pega primeira aeronave que aparece
                sc.nextLine();

                CiaAerea cia = gerCias.buscarCodigo(cod);
                Aeroporto origem = gerAero.buscarCodigo(codAeroOrigem);
                Aeroporto destino = gerAero.buscarCodigo(codAeroDestino);
                Aeronave aeronave = gerAvioes.buscarCodigo(codAeronaves);

                Rota r = new Rota(cia, origem, destino, aeronave);
                System.out.println("Carregando Rota no Gerenciador de Rotas: " + r);
                adicionar(r);
            }
        }
    }

    public void ordenarCias() {
        Collections.sort(rotas);
    }

    public void ordenarNomesCias() {
        rotas.sort((Rota r1, Rota r2) ->
                r1.getCia().getNome().compareTo(
                        r2.getCia().getNome()));
    }

    public void ordenarNomesAeroportos() {
        rotas.sort(Comparator.comparing((Rota r) -> r.getOrigem().getCodigo()));
    }

    public void ordenarNomesAeroportosCias() {
        rotas.sort((Rota r1, Rota r2) -> {
            int result = r1.getOrigem().getNome().compareTo(
                    r2.getOrigem().getNome());
            if (result != 0)
                return result;
            return r1.getCia().getNome().compareTo(
                    r2.getCia().getNome());
        });
    }

    public void adicionar(Rota r) {
        rotas.add(r);
    }

    public ArrayList<Rota> listarTodas() {
        return new ArrayList<Rota>(rotas);
    }

    public ArrayList<Rota> buscarOrigem(String codigo) {
        ArrayList<Rota> result = new ArrayList<>();
        for (Rota r : rotas)
            if (r.getOrigem().getCodigo().equals(codigo))
                result.add(r);
        return result;
    }

//  4.  Selecionar um aeroporto de origem e mostrar todos os aeroportos que são alcançáveis até um determinado tempo de vôo
//            (ex: 12 horas), com no máximo duas conexões.


    public void listaAeroportoPorTempo(Aeroporto origem, Voo duracao) {
        origem.getLocal();
    }

}