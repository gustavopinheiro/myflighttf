package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import java.time.Duration;

public class GerenciadorAeroportos {

    private HashMap<String, Aeroporto> aeroportos;

    public GerenciadorAeroportos() {
        this.aeroportos = new HashMap<>();
    }

    public void carregaDados() throws IOException {
        //System.out.println("### Carregando Aeroportos ###\n\n");
        Path path2 = Paths.get("airports.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path2, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            String header = sc.nextLine(); // pula cabeçalho
            String codigo, nome, pais;
            String slat, slon;
            while (sc.hasNext()) {
                codigo = sc.next();
                slat = sc.next();
                slon = sc.next();
                nome = sc.next();
                pais = sc.next().replaceAll("(\r)", ""); // tem um \r escondido no fim da linha
                Geo geo = new Geo(Double.parseDouble(slat), Double.parseDouble(slon));
                Aeroporto nova = new Aeroporto(codigo, nome, geo, pais);
                adicionar(nova);
                System.out.println("Carregando no Gerenciador de Aeroportos: " + nova);
            }
        }
    }

    public void adicionar(Aeroporto aero) {
        aeroportos.put(aero.getCodigo(), aero);
    }

    public ArrayList<Aeroporto> listarTodas() {
        return new ArrayList<>(aeroportos.values());
    }

    public Aeroporto buscarCodigo(String codigo) {
        return aeroportos.get(codigo);
    }
}
