package pucrs.myflight.modelo;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GerenciadorPaises {

    private HashMap<String, Pais> paises;

    public GerenciadorPaises() {
        this.paises = new HashMap<>();
    }

    public void carregaDados() throws IOException {
        Path path = Paths.get("countries.dat");
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, Charset.forName("utf8")))) {
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            String header = sc.nextLine(); // pula cabeçalho
            String cod, nome;
            while (sc.hasNext()) {
                cod = sc.next();
                nome = sc.next();
                Pais nova = new Pais(cod, nome);
                adicionar(nova);
                System.out.println("Carregando no Gerenciador de Paises: " + nova);
            }
        }
    }

    public void adicionar(Pais paise) {
        paises.put(paise.getCod(), paise);
    }

    public ArrayList<Pais> listarTodos() {
        return new ArrayList<>(paises.values());
    }

    public Pais buscarCodigo(String codigo) {
        return paises.get(codigo);
    }

    public ArrayList<Pais> listarTodasOrdenadaPorNome() {
        ArrayList<Pais> lista = listarTodos();
        lista.sort( (Pais a, Pais b)
                -> a.getNome().compareTo(b.getNome()));
        return lista;
    };
}
