package pucrs.myflight.modelo;

import java.util.ArrayList;

public class Pais implements Comparable<Pais> {
    private String cod;
    private String nome;
    private ArrayList<Aeroporto> aeroportos;

    public Pais(String cod) {
        this.cod = cod;
    }

    public Pais(String cod, String nome) {
        this.cod = cod;
        this.nome = nome;
        this.aeroportos = new ArrayList<Aeroporto>();
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<Aeroporto> getAeroportos() {
        return aeroportos;
    }

    @Override
    public String toString() {
        return "(" + cod + ") " + nome;
    }

    @Override
    public int compareTo(Pais outro) {
        return this.nome.compareTo(outro.nome);
    }
}
