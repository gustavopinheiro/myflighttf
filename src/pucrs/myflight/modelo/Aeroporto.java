package pucrs.myflight.modelo;

public class Aeroporto implements Comparable<Aeroporto> {
	private String codigo;
	private String nome;
	private Geo loc;
	private String codPais;
	
	public Aeroporto(String codigo, String nome, Geo loc, String countyCod) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
        this.codPais = countyCod;
	}

	public Aeroporto(String codigo, String nome, double latitude, double longitude, String countryCod){
		this.codigo = codigo;
		this.nome = nome;
		this.loc = new Geo(latitude, longitude);
		this.codPais = countryCod;
	}

    public Aeroporto(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	}

	public String getCodPais() {
		return codPais;
	}

	@Override
    public String toString() {
		if(codPais == null) {
			return "(Sem País) " + codigo + " - " + nome + "[" + loc + "]";
		}
		if(loc == null) {
			return "("+ codPais+") " + codigo + " - " + nome + " [Sem Localização]";
		}
        return "("+ codPais +") " + codigo + " - " + nome + " [" + loc + "]";
    }

	@Override
	public int compareTo(Aeroporto outro) {
		return this.nome.compareTo(outro.nome);
	}

}
